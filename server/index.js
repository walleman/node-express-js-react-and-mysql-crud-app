const express =require("express");
const app =express();
const cors=require("cors");
const bodyParser=require("body-parser");
const mysql=require("mysql");

const db= mysql.createPool({
    host:"localhost",
    user:"root",
    password:"1234",
    database:"expressdb",
    port:"3308"
});
 app.use(cors());
 app.use(express.json());

app.use(bodyParser.urlencoded({extended:true}));

app.get("/api/get",(req,res)=>{
    const sqlGet="Select * from contacts";
    db.query(sqlGet,(error,result)=>{
         res.send(result);
    });
});

app.post("/api/post",(req,res)=>{
        const {name,email,contact}=req.body;
        const sqlInsert="INSERT INTO contacts (name,email,contact) VALUES (?, ?, ?)";
        db.query(sqlInsert,[name,email,contact],(error,result)=>{
           if(error){
                console.log(error);
           }
       });
});
app.delete("/api/remove/:id",(req,res)=>{
         const {id} = req.params;
          const sqlDelete="Delete from contacts where id=?";
          db.query(sqlDelete,id,(error,result)=>{
            if(error){
                console.log(error);
            };
          });
});

app.get("/api/get/:id",(req,res)=>{
    const {id} = req.params;
     const sqlGet="Select * from contacts where id=?";
     db.query(sqlGet,id,(error,result)=>{
       if(error){
           console.log(error);
       }
       res.send(result);
     });
});

app.put("/api/update/:id",(req,res)=>{
      const {id}=req.params;
      const {name,email,contact} =req.body;
      const sqlUpdate="Update contacts Set name=?, email=?, contact=? where id=?";
    db.query(sqlUpdate,[name,email,contact,id],(error,result)=>{
       if(error){
        console.log(error);
        }
        res.send(result);
      });
});

app.get("/",(req,res)=>{
    // const sqlInsert="Insert into contacts (name,email,contact) VALUES ('walleman','walle@gmail.com','123534645')";
    // db.query(sqlInsert,(error,result)=>{
    //     console.log("error",error);
    //     console.log("result",result);
    // });
    //  res.send("hello express");
});
app.listen(5000,()=>{
    console.log("server starting on port 5000");
});